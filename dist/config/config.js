"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var dbuser = 'admin';
var dbpass = 'admin';
var dbname = 'test-db';
exports.default = {
    jwtSecret: 'mySecret',
    mongoURL: process.env.MONGODB_URI || "mongodb://" + dbuser + ":" + dbuser + "@ds129610.mlab.com:29610/" + dbname,
    port: process.env.PORT || 3000
};
