"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var DesignComponentSchema = new Schema({
    design_name: String,
    component_id: { type: Schema.Types.ObjectId, ref: 'Component', required: true },
    file_name: String,
    create_time: {
        type: Date,
        default: Date.now
    }
});
exports.default = mongoose.model('DesignComponent', DesignComponentSchema);
