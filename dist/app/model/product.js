"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ProductSchema = new Schema({
    product_name: String,
    product_desc: String,
    product_location: String,
    create_time: {
        type: Date,
        default: Date.now
    }
});
exports.default = mongoose.model('Product', ProductSchema);
