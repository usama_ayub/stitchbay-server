"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ComponentSchema = new Schema({
    component_name: String,
    product_id: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
    component_location: String,
    create_time: {
        type: Date,
        default: Date.now
    }
});
/* const ComponentSchema = new Schema({
    componentName: String,
    componentColor: String,
    productID: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
    componentLocation: String,
    createTime: {
        type: Date,
        default: Date.now
    }
}); */
exports.default = mongoose.model('Component', ComponentSchema);
