"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var MeasurementComponentSchema = new Schema({
    product_id: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
    measurement_name: String,
    measurement_img: String,
    measurement_gif: String,
    measurement_location: String,
    create_time: {
        type: Date,
        default: Date.now
    }
});
exports.default = mongoose.model('MeasurementComponent', MeasurementComponentSchema);
