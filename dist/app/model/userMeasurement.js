"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var UserMeasurementSchema = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    measurement: [{
            id: { type: Schema.Types.ObjectId, ref: 'MeasurementComponent', required: true },
            value: Number,
        }],
    create_time: {
        type: Date,
        default: Date.now
    }
});
/* const userMeasurement = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    measurement: [{
        id: { type: Schema.Types.ObjectId, ref: 'MeasurementComponent', required: true },
        measures: Number,
    }],
    create_time: {
        type: Date,
        default: Date.now
    }
});
 */
exports.default = mongoose.model('UserMeasurement', UserMeasurementSchema);
