"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var UserProductSchema = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    product_id: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
    //component_id: { type: Schema.Types.ObjectId, ref: 'Component', required: true },
    designComponent: [{
            _id: {
                type: Schema.Types.ObjectId,
                ref: 'DesignComponent',
                required: true
            }
        }],
    userMeasurement_id: { type: Schema.Types.ObjectId, ref: 'UserMeasurement', required: true },
    cost: Number,
    create_time: {
        type: Date,
        default: Date.now
    }
});
exports.default = mongoose.model('UserProduct', UserProductSchema);
