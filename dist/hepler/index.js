"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function responseJsonHandler(err, data, res, meg) {
    (err) ? res.json({ success: false, 'data': null, err: err }) : res.json({ success: true, data: data, 'err': null, message: meg });
}
exports.responseJsonHandler = responseJsonHandler;
