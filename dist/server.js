"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var cors = require("cors");
// config
var config_1 = require("./config/config");
var app = express();
var port = config_1.default.port;
// server routes
var user_1 = require("./api/user");
var auth_1 = require("./api/auth");
var product_1 = require("./api/product");
var component_1 = require("./api/component");
var design_component_1 = require("./api/design_component");
var measurement_component_1 = require("./api/measurement_component");
// mongoose connection check
mongoose.connect(config_1.default.mongoURL);
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to');
});
mongoose.connection.on('error', function (err) {
    console.log("Mongoose default connection error: " + err);
});
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});
// App
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
// Api
app.use('/api/auth', auth_1.default);
app.use('/api/user', user_1.default);
app.use('/api/product', product_1.default);
app.use('/api/component', component_1.default);
app.use('/api/design_component', design_component_1.default);
app.use('/api/measurement_componentRoutes', measurement_component_1.default);
// Port 
app.listen(port, function () {
    console.log("Server Running  " + port);
});
;
