"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var userMeasurement_1 = require("../../app/model/userMeasurement");
function add(obj, cb) {
    var userMeasurement = new userMeasurement_1.default(obj);
    userMeasurement.save(cb);
}
exports.add = add;
function getById(obj, cb) {
    var query = userMeasurement_1.default.findById(obj);
    query.exec(cb);
}
exports.getById = getById;
function getByUserId(obj, cb) {
    var query = userMeasurement_1.default.findById(obj);
    query.exec(cb);
}
exports.getByUserId = getByUserId;
