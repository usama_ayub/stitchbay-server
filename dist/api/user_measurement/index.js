"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var controller_1 = require("./controller");
var router = express.Router();
//post
router.get('/addUserMeasurement', controller_1.addUserMeasurement);
//get
router.get('/', controller_1.testApi);
router.get('/getMeasurementById/:_id', controller_1.getMeasurementById);
router.get('/getMeasurementByUserId/user_id', controller_1.getMeasurementByUserId);
//put
router.put('/updateMeasurementById', controller_1.updateMeasurementById);
exports.default = router;
