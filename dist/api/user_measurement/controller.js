"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var hepler_1 = require("../../hepler");
var method_1 = require("../user_measurement/method");
function testApi(req, res, next) {
    res.json({ 'User Measurement': 'User Measurement API Working' });
}
exports.testApi = testApi;
function addUserMeasurement(req, res, next) {
    var body = req.body;
    var user_id = body.user_id, measurement = body.measurement;
    method_1.add(body, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'add measurement');
    });
}
exports.addUserMeasurement = addUserMeasurement;
function getMeasurementById(req, res, next) {
    var params = req.params;
    var _id = params._id;
    method_1.getById(params._id, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'get measurement by given id');
    });
}
exports.getMeasurementById = getMeasurementById;
function getMeasurementByUserId(req, res, next) {
    var params = req.params;
    var user_id = params.user_id;
    method_1.getByUserId(params.user_id, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'add measurement by user id');
    });
}
exports.getMeasurementByUserId = getMeasurementByUserId;
function updateMeasurementById(req, res, next) {
    //
}
exports.updateMeasurementById = updateMeasurementById;
