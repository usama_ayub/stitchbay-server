"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var hepler_1 = require("../../hepler");
var method_1 = require("../design_component/method");
function testApi(req, res, next) {
    res.json({ 'Component Design': ' Component Design API Working' });
}
exports.testApi = testApi;
function addDesignComponent(req, res, next) {
    var body = req.body;
    var design_name = body.design_name, component_id = body.component_id, file_name = body.file_name;
    method_1.add(body, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'add component design');
    });
}
exports.addDesignComponent = addDesignComponent;
function getComponentByColor(req, res, next) {
    var params = req.params.color;
    // let { product_id } = params;
    method_1.getByColor(params, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'get design component by color');
    });
}
exports.getComponentByColor = getComponentByColor;
