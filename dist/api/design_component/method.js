"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var designComponent_1 = require("../../app/model/designComponent");
function getByColor(obj, cb) {
    var query = designComponent_1.default.find({ design_name: obj });
    query.populate('component_id', 'component_location');
    query.exec(cb);
}
exports.getByColor = getByColor;
function add(obj, cb) {
    var designComponent = new designComponent_1.default(obj);
    designComponent.save(cb);
}
exports.add = add;
