"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var controller_1 = require("./controller");
var router = express.Router();
//post
router.post('/addDesignComponent', controller_1.addDesignComponent);
//get
router.get('/', controller_1.testApi);
router.get('/getComponentByColor/:color', controller_1.getComponentByColor);
exports.default = router;
