"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var hepler_1 = require("../../hepler");
var method_1 = require("../measurement_component/method");
function testApi(req, res, next) {
    res.json({ 'Measurement': 'Measurement API Working' });
}
exports.testApi = testApi;
function addMeasurement(req, res, next) {
    var body = req.body;
    var product_id = body.product_id, measurement_name = body.measurement_name, measurement_img = body.measurement_img, measurement_gif = body.measurement_gif, measurement_location = body.measurement_location;
    method_1.add(body, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'add measurement');
    });
}
exports.addMeasurement = addMeasurement;
function getMeasurementByProductId(req, res, next) {
    var params = req.params;
    var product_id = params.product_id;
    method_1.getByProductId(params.product_id, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'get measurement by product id');
    });
}
exports.getMeasurementByProductId = getMeasurementByProductId;
