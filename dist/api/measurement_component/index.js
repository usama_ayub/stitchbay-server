"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var controller_1 = require("./controller");
var router = express.Router();
// Post
router.get('/addMeasurement', controller_1.addMeasurement);
//Get
router.get('/', controller_1.testApi);
router.get('/getMeasurementByProductId/:product_id', controller_1.getMeasurementByProductId);
exports.default = router;
