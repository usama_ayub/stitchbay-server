"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var measurementComponent_1 = require("../../app/model/measurementComponent");
function getByProductId(obj, cb) {
    var query = measurementComponent_1.default.findById(obj);
    query.exec(cb);
}
exports.getByProductId = getByProductId;
function add(obj, cb) {
    var measurementComponent = new measurementComponent_1.default(obj);
    measurementComponent.save(cb);
}
exports.add = add;
