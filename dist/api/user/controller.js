"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var user_1 = require("../../app/model/user");
var hepler_1 = require("../../hepler");
var method_1 = require("../user/method");
function testApi(req, res, next) {
    res.json({ 'user': 'API Working' });
}
exports.testApi = testApi;
function getAllUser(req, res, next) {
    method_1.get({}, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res);
    });
    /* User.find({}, (err, users) => {
        if (err) {
            return res.json({ success: false, data: null, error: 'Users Not Found' })
        } else {
            return res.json({ success: true, data: users, error: null })
        }
    }) */
}
exports.getAllUser = getAllUser;
function getUserById(req, res, next) {
    var params = req.params;
    var user_id = params.user_id;
    user_1.default.findById(params.user_id, function (err, user) {
        if (err) {
            return res.json({ success: false, data: null, error: 'User Not Found' });
        }
        else {
            return res.json({ success: true, data: user, error: null });
        }
    });
}
exports.getUserById = getUserById;
function userUpdateProfile(req, res, next) {
    console.log(req.file);
    var body = req.body;
    var user_id = body.user_id, userName = body.userName, profileName = body.profileName, firstName = body.firstName, lastName = body.lastName;
    body.profileName = req.file.destination + req.file.originalname;
    var data = {
        userName: body.userName,
        profileName: body.profileName,
        firstName: body.firstName,
        lastName: body.lastName
    };
    user_1.default.findByIdAndUpdate(body.user_id, data, function (err, update) {
        if (err) {
            return res.json({ success: false, data: null, error: err });
        }
        else {
            return res.json({ success: true, data: update, error: null });
        }
    });
}
exports.userUpdateProfile = userUpdateProfile;
function changePassword(req, res, next) {
    /*     let body = req.body;
        let { user_id, oldPassword, newPassword } = body;
        User.findOne({ _id: body.user_id }, (err, user) => {
            if (err) return res.json({ success: false, data: null, error: err });
            if (!user) return res.json({ success: false, data: null, error: 'User is invalid' });
            if (user.comparePassword(oldPassword)) {
                user.password = newPassword
                user.save((err) => {
                    if (err) {
                        return res.json({ success: false, data: null, error: err });
                    } else {
                        return res.json({ success: true, data: "Password Successful Change", error: null });
                    }
                });
            }
            else {
                return res.json({ success: false, data: null, error: 'Old Password is not Match' });
            }
        }); */
}
exports.changePassword = changePassword;
