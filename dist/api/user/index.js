"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var multer = require("multer");
var controller_1 = require("./controller");
var router = express.Router();
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'profile/');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
var upload = multer({ storage: storage });
router.get('/', controller_1.testApi);
router.get('/getUser', controller_1.getAllUser);
router.get('/user/:user_id', controller_1.getUserById);
router.put('/user/updateProfile', upload.single('img'), controller_1.userUpdateProfile);
router.post('/user/changePassword', controller_1.changePassword);
exports.default = router;
