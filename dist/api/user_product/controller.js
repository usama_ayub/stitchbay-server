"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var hepler_1 = require("../../hepler");
var method_1 = require("../user_product/method");
function testApi(req, res, next) {
    res.json({ 'User Product': 'User Product API Working' });
}
exports.testApi = testApi;
function addUserProduct(req, res, next) {
    var body = req.body;
    var user_id = body.user_id, product_id = body.product_id, designComponent = body.designComponent, userMeasurement_id = body.userMeasurement_id, cost = body.cost, create_time = body.create_time;
    method_1.add(body, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'add user product');
    });
}
exports.addUserProduct = addUserProduct;
function getUserProductByUserId(req, res, next) {
    var params = req.params;
    var _id = params._id;
    method_1.getByUserId(params.product_id, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'get User Product by user id');
    });
}
exports.getUserProductByUserId = getUserProductByUserId;
function getUserProductById(req, res, next) {
    var params = req.params;
    var user_id = params.user_id;
    method_1.getById(params.product_id, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'get User Product by id');
    });
}
exports.getUserProductById = getUserProductById;
function getUserProductByProductId(req, res, next) {
    var params = req.params;
    var product_id = params.product_id;
    method_1.getProductId(params.product_id, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'get User Product by product id');
    });
}
exports.getUserProductByProductId = getUserProductByProductId;
