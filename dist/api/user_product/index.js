"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var controller_1 = require("./controller");
var router = express.Router();
// Post
router.get('/addUserProduct', controller_1.addUserProduct);
// get
router.get('/', controller_1.testApi);
router.get('/getUserProductByUserId/:user_id', controller_1.getUserProductByUserId);
router.get('/getUserProductById/:_id', controller_1.getUserProductById);
router.get('/getUserProductByProductId/:product_id', controller_1.getUserProductByProductId);
exports.default = router;
