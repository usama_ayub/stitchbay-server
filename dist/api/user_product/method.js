"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var userProduct_1 = require("../../app/model/userProduct");
function getById(obj, cb) {
    var query = userProduct_1.default.findById(obj);
    query.exec(cb);
}
exports.getById = getById;
function getByUserId(obj, cb) {
    var query = userProduct_1.default.findById(obj);
    query.exec(cb);
}
exports.getByUserId = getByUserId;
function getProductId(obj, cb) {
    var query = userProduct_1.default.findById(obj);
    query.exec(cb);
}
exports.getProductId = getProductId;
function add(obj, cb) {
    var userProduct = new userProduct_1.default(obj);
    userProduct.save(cb);
}
exports.add = add;
