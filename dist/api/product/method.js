"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var product_1 = require("../../app/model/product");
function get(obj, cb) {
    var query = product_1.default.find(obj);
    query.exec(cb);
}
exports.get = get;
function add(obj, cb) {
    var product = new product_1.default(obj);
    product.save(cb);
}
exports.add = add;
function getById(obj, cb) {
    var query = product_1.default.findById(obj);
    query.exec(cb);
}
exports.getById = getById;
