"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var hepler_1 = require("../../hepler");
var method_1 = require("../product/method");
function testApi(req, res, next) {
    res.json({ 'Product': 'Product API Working' });
}
exports.testApi = testApi;
function addProduct(req, res, next) {
    var body = req.body;
    var product_name = body.product_name, product_desc = body.product_desc, product_location = body.product_location;
    method_1.add(body, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'add products');
    });
}
exports.addProduct = addProduct;
function getAllProduct(req, res, next) {
    method_1.get({}, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'get all product');
    });
}
exports.getAllProduct = getAllProduct;
function getProductById(req, res, next) {
    var params = req.params.product_id;
    method_1.getById(params, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'get product by given Id');
    });
}
exports.getProductById = getProductById;
