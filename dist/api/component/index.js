"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var controller_1 = require("./controller");
var router = express.Router();
// Post
router.post('/addComponent', controller_1.addComponent);
// Get
router.get('/', controller_1.testApi);
router.get('/getAllComponent', controller_1.getAllComponent);
router.get('/getAllComponentByProductId/:product_id', controller_1.getAllComponentByProductId);
exports.default = router;
