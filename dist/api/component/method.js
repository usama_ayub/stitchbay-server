"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = require("../../app/model/component");
function get(obj, cb) {
    var query = component_1.default.find(obj);
    query.exec(cb);
}
exports.get = get;
function add(obj, cb) {
    var component = new component_1.default(obj);
    component.save(cb);
}
exports.add = add;
function getComponentByProductId(obj, cb) {
    var query = component_1.default.findById(obj);
    query.exec(cb);
}
exports.getComponentByProductId = getComponentByProductId;
