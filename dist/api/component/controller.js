"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var hepler_1 = require("../../hepler");
var method_1 = require("../component/method");
function testApi(req, res, next) {
    res.json({ 'component': 'Component API Working' });
}
exports.testApi = testApi;
function addComponent(req, res, next) {
    var body = req.body;
    var component_name = body.component_name, product_id = body.product_id, component_location = body.component_location;
    method_1.add(body, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'add component');
    });
}
exports.addComponent = addComponent;
function getAllComponent(req, res, next) {
    method_1.get({}, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'get all component');
    });
}
exports.getAllComponent = getAllComponent;
function getAllComponentByProductId(req, res, next) {
    var params = req.params;
    var product_id = params.product_id;
    method_1.getComponentByProductId(params.product_id, function (err, data) {
        hepler_1.responseJsonHandler(err, data, res, 'get component by product Id');
    });
}
exports.getAllComponentByProductId = getAllComponentByProductId;
