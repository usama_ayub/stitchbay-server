import express = require("express");

//create type for callback function
export type CallBackFunction = (error, data) => void

export function responseJsonHandler(err, data, res: express.Response, meg?) {
    (err) ? res.json({ success: false, 'data': null, err }) : res.json({ success: true, data, 'err': null, message: meg });
}