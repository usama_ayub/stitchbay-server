import * as express from 'express';
import * as multer from 'multer';
import * as controller from './controller';
const router = express.Router();

// Post
router.get('/addMeasurement', controller.addMeasurement);

//Get
router.get('/', controller.testApi);
router.get('/getMeasurementByProductId/:product_id', controller.getMeasurementByProductId);



export default router;