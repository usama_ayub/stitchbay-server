import * as express from 'express';
import { responseJsonHandler } from '../../hepler';
import * as method from '../measurement_component/method';

export function testApi(req, res, next) {
    res.json({ 'Measurement': 'Measurement API Working' });
}

export function addMeasurement(req, res, next) {
    let body = req.body;
    let { product_id, measurement_name, measurement_img, measurement_gif, measurement_location } = body;
    method.add(body, (err, data) => {
        responseJsonHandler(err, data, res, 'add measurement');
    });
}

export function getMeasurementByProductId(req, res, next) {
    let params = req.params;
    let { product_id } = params;
    method.getByProductId(params.product_id, (err, data) => {
        responseJsonHandler(err, data, res, 'get measurement by product id');
    });
}
