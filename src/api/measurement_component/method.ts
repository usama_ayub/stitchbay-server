import * as express from 'express';
import MeasurementComponent from '../../app/model/measurementComponent';
import { CallBackFunction } from '../../hepler';

export function getByProductId(obj, cb: CallBackFunction) {
    let query = MeasurementComponent.findById(obj)
    query.exec(cb);
}

export function add(obj, cb: CallBackFunction) {
    let measurementComponent = new MeasurementComponent(obj);
    measurementComponent.save(cb);
}