import * as express from 'express';
import * as multer from 'multer';
import * as controller from './controller'
const router = express.Router();


//post
router.post('/addDesignComponent', controller.addDesignComponent);

//get
router.get('/', controller.testApi);
router.get('/getComponentByColor/:color', controller.getComponentByColor);




export default router;