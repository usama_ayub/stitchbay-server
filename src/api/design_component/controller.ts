import * as express from 'express';
import { responseJsonHandler } from '../../hepler';
import * as method  from '../design_component/method';

export function testApi(req, res, next) {
    res.json({ 'Component Design': ' Component Design API Working' });
}

export function addDesignComponent(req, res, next) {
    let body = req.body;
    let { design_name, component_id, file_name } = body;
    method.add(body, (err, data) => {
        responseJsonHandler(err, data, res, 'add component design');
    });
}


export function getComponentByColor(req, res, next) {
    let params = req.params.color;
    // let { product_id } = params;
    method.getByColor(params, (err, data) => {
        responseJsonHandler(err, data, res, 'get design component by color');
    });
}