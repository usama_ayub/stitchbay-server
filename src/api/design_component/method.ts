import * as express from 'express';
import DesignComponent from '../../app/model/designComponent';
import { CallBackFunction } from '../../hepler';

export function getByColor(obj, cb: CallBackFunction) {
    let query = DesignComponent.find({ design_name: obj })
    query.populate('component_id', 'component_location')
    query.exec(cb);
}

export function add(obj, cb: CallBackFunction) {
    let designComponent = new DesignComponent(obj);
    designComponent.save(cb);
}