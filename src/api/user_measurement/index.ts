import * as express from 'express';
import * as multer from 'multer';
import * as controller from './controller'
const router = express.Router();


//post
router.get('/addUserMeasurement', controller.addUserMeasurement);

//get
router.get('/', controller.testApi);
router.get('/getMeasurementById/:_id', controller.getMeasurementById);
router.get('/getMeasurementByUserId/user_id', controller.getMeasurementByUserId);

//put
router.put('/updateMeasurementById', controller.updateMeasurementById);



export default router;