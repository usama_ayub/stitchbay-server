import * as express from 'express';
import { responseJsonHandler } from '../../hepler';
import * as method from '../user_measurement/method';

export function testApi(req, res, next) {
    res.json({ 'User Measurement': 'User Measurement API Working' });
}

export function addUserMeasurement(req, res, next) {
    let body = req.body;
    let { user_id, measurement } = body;
    method.add(body, (err, data) => {
        responseJsonHandler(err, data, res, 'add measurement');
    });
}

export function getMeasurementById(req, res, next) {
    let params = req.params;
    let { _id } = params;
    method.getById(params._id, (err, data) => {
        responseJsonHandler(err, data, res, 'get measurement by given id');
    });
}

export function getMeasurementByUserId(req, res, next) {
    let params = req.params;
    let { user_id } = params;
    method.getByUserId(params.user_id, (err, data) => {
        responseJsonHandler(err, data, res, 'add measurement by user id');
    });
}

export function updateMeasurementById(req, res, next) {
    //
}