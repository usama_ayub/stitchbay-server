import * as express from 'express';
import UserMeasurement from '../../app/model/userMeasurement';
import { CallBackFunction } from '../../hepler';

export function add(obj, cb: CallBackFunction) {
    let userMeasurement = new UserMeasurement(obj);
    userMeasurement.save(cb);
}

export function getById(obj, cb: CallBackFunction) {
    let query = UserMeasurement.findById(obj)
    query.exec(cb);
}
export function getByUserId(obj, cb: CallBackFunction) {
    let query = UserMeasurement.findById(obj)
    query.exec(cb);
}
