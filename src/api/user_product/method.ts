import * as express from 'express';
import UserProduct from '../../app/model/userProduct';
import { CallBackFunction } from '../../hepler';


export function getById(obj, cb: CallBackFunction) {
    let query = UserProduct.findById(obj)
    query.exec(cb);
}

export function getByUserId(obj, cb: CallBackFunction) {
    let query = UserProduct.findById(obj)
    query.exec(cb);
}

export function getProductId(obj, cb: CallBackFunction) {
    let query = UserProduct.findById(obj)
    query.exec(cb);
}

export function add(obj, cb: CallBackFunction) {
    let userProduct = new UserProduct(obj);
    userProduct.save(cb);
}
