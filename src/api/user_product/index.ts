import * as express from 'express';
import * as controller from './controller'
const router = express.Router();


// Post
router.get('/addUserProduct', controller.addUserProduct);

// get
router.get('/', controller.testApi);
router.get('/getUserProductByUserId/:user_id', controller.getUserProductByUserId);
router.get('/getUserProductById/:_id', controller.getUserProductById);
router.get('/getUserProductByProductId/:product_id', controller.getUserProductByProductId);



export default router;