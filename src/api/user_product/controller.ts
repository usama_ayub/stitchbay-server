import * as express from 'express';
import { responseJsonHandler } from '../../hepler';
import * as method from '../user_product/method';

export function testApi(req, res, next) {
    res.json({ 'User Product': 'User Product API Working' });
}

export function addUserProduct(req, res, next) {
    let body = req.body;
    let { user_id, product_id, designComponent, userMeasurement_id, cost, create_time } = body;
    method.add(body, (err, data) => {
        responseJsonHandler(err, data, res, 'add user product');
    });
}

export function getUserProductByUserId(req, res, next) {
    let params = req.params;
    let { _id } = params;
    method.getByUserId(params.product_id, (err, data) => {
        responseJsonHandler(err, data, res, 'get User Product by user id');
    });
}

export function getUserProductById(req, res, next) {
    let params = req.params;
    let { user_id } = params;
    method.getById(params.product_id, (err, data) => {
        responseJsonHandler(err, data, res, 'get User Product by id');
    });
}

export function getUserProductByProductId(req, res, next) {
    let params = req.params;
    let { product_id } = params;
    method.getProductId(params.product_id, (err, data) => {
        responseJsonHandler(err, data, res, 'get User Product by product id');
    });
}