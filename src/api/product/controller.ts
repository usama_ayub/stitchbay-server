import * as express from 'express';
import { responseJsonHandler } from '../../hepler';
import * as method from '../product/method';

export function testApi(req, res, next) {
    res.json({ 'Product': 'Product API Working' });
}

export function addProduct(req, res, next) {
    let body = req.body;
    let { product_name, product_desc, product_location } = body;
    method.add(body, (err, data) => {
        responseJsonHandler(err, data, res, 'add products');
    });
}

export function getAllProduct(req, res, next) {
    method.get({}, (err, data) => {
        responseJsonHandler(err, data, res, 'get all product');
    });
}

export function getProductById(req, res, next) {
    let params = req.params.product_id;
    method.getById(params, (err, data) => {
        responseJsonHandler(err, data, res, 'get product by given Id');
    });
}




