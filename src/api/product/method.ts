import * as express from 'express';
import * as shortid from 'shortid';
import * as nodemailer from 'nodemailer';
import Product from '../../app/model/product';
import config from '../../config/config';
import { CallBackFunction } from '../../hepler';

export function get(obj, cb: CallBackFunction) {
    let query = Product.find(obj)
    query.exec(cb);
}

export function add(obj, cb: CallBackFunction) {
    let product = new Product(obj);
    product.save(cb);
}

export function getById(obj, cb: CallBackFunction) {
    let query = Product.findById(obj)
    query.exec(cb);
}