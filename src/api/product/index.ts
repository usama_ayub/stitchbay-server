import * as express from 'express';
import * as multer from 'multer';
import * as controller from './controller'
const router = express.Router();


router.get('/', controller.testApi);
router.get('/getAllProduct', controller.getAllProduct);
router.post('/addProduct', controller.addProduct);
router.get('/getProductById/:product_id', controller.getProductById);



export default router;