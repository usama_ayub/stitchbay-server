import * as express from 'express';
import * as shortid from 'shortid';
import * as nodemailer from 'nodemailer';
import User from '../../app/model/user';
import config from '../../config/config';
import { CallBackFunction } from '../../hepler';

export function get(obj, cb: CallBackFunction) {
    let query = User.find(obj)
    query.exec(cb);
}