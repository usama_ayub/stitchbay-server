import * as express from 'express';
import { responseJsonHandler } from '../../hepler';
import * as method from '../component/method';

export function testApi(req, res, next) {
    res.json({ 'component': 'Component API Working' });
}

export function addComponent(req, res, next) {
    let body = req.body;
    let { component_name, product_id, component_location } = body;
    method.add(body, (err, data) => {
        responseJsonHandler(err, data, res, 'add component');
    });
}

export function getAllComponent(req, res, next) {
    method.get({}, (err, data) => {
        responseJsonHandler(err, data, res, 'get all component');
    });
}

export function getAllComponentByProductId(req, res, next) {
    let params = req.params;
    let { product_id } = params;
    method.getComponentByProductId(params.product_id, (err, data) => {
        responseJsonHandler(err, data, res, 'get component by product Id');
    });
}




