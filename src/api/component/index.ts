import * as express from 'express';
import * as controller from './controller'
const router = express.Router();


// Post
router.post('/addComponent', controller.addComponent);

// Get
router.get('/', controller.testApi);
router.get('/getAllComponent', controller.getAllComponent);
router.get('/getAllComponentByProductId/:product_id', controller.getAllComponentByProductId);



export default router;