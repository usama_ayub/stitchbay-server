import * as express from 'express';
import * as shortid from 'shortid';
import * as nodemailer from 'nodemailer';
import Component from '../../app/model/component';
import config from '../../config/config';
import { CallBackFunction } from '../../hepler';

export function get(obj, cb: CallBackFunction) {
    let query = Component.find(obj)
    query.exec(cb);
}

export function add(obj, cb: CallBackFunction) {
    let component = new Component(obj);
    component.save(cb);
}

export function getComponentByProductId(obj, cb: CallBackFunction) {
    let query = Component.findById(obj)
    query.exec(cb);
}