import * as express from 'express';
import * as mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import * as fs from 'fs';
import * as Busboy from 'busboy';
import * as randomstring from 'randomstring';
import * as mime from 'mime-types';
import * as cors from 'cors';
import * as shortid from 'shortid';
import * as nodemailer from 'nodemailer';
import * as multer from 'multer';


// config
import config from './config/config';
const app = express();
const port = config.port;

// server routes
import userRoutes from './api/user';
import authRoutes from './api/auth';
import productRoutes from './api/product';
import componentRoutes from './api/component';
import design_componentRoutes from './api/design_component';
import measurement_componentRoutes from './api/measurement_component';




// mongoose connection check
mongoose.connect(config.mongoURL);
mongoose.connection
    .once('open', () => console.log('mongodb run'))
    .on('err', err => console.error(err));


// App
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());


// Api
app.use('/api/auth', authRoutes);
app.use('/api/user', userRoutes);
app.use('/api/product', productRoutes);
app.use('/api/component', componentRoutes);
app.use('/api/design_component', design_componentRoutes);
app.use('/api/measurement_componentRoutes', measurement_componentRoutes);

// Port 
app.listen(port, () => {
    console.log(`Server Running  ${port}`);
});
;
