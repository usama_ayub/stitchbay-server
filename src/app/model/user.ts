import * as mongoose from 'mongoose';
const bcrypt = require('bcrypt');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: String,
    password: String,
    phone_number: Number,
    create_time: {
        type: Date,
        default: Date.now
    }
});


UserSchema.pre('save', function (next) {
    let user = this;
    if (!user.isModified('password')) return next();
    let hash = bcrypt.hashSync(user.password, 5);
    user.password = hash;
    next();
});


UserSchema.methods = {
    comparePassword(password, cb) {
        let isMatch = bcrypt.compareSync(password, this.password)
        return isMatch
    }
}

export default mongoose.model('User', UserSchema);