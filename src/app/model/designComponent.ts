import * as mongoose from 'mongoose';
import Component from '../../app/model/component';

const Schema = mongoose.Schema;

const DesignComponentSchema = new Schema({
    design_name: String,
    component_id: { type: Schema.Types.ObjectId, ref: 'Component', required: true },
    file_name: String,
    create_time: {
        type: Date,
        default: Date.now
    }
});



export default mongoose.model('DesignComponent', DesignComponentSchema);