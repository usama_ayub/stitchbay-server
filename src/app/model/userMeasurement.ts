import * as mongoose from 'mongoose';
import User from '../../app/model/user';
import MeasurementComponent from '../../app/model/measurementComponent';

const Schema = mongoose.Schema;

const UserMeasurementSchema = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    measurement: [{
        id: { type: Schema.Types.ObjectId, ref: 'MeasurementComponent', required: true },
        value: Number,
    }],
    create_time: {
        type: Date,
        default: Date.now
    }
});

/* const userMeasurement = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    measurement: [{
        id: { type: Schema.Types.ObjectId, ref: 'MeasurementComponent', required: true },
        measures: Number,
    }],
    create_time: {
        type: Date,
        default: Date.now
    }
});
 */
export default mongoose.model('UserMeasurement', UserMeasurementSchema);