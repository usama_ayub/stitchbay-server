import * as mongoose from 'mongoose';
import Product from '../../app/model/product'

const Schema = mongoose.Schema;

const ComponentSchema = new Schema({
    component_name: String,
    product_id: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
    component_location: String,
    create_time: {
        type: Date,
        default: Date.now
    }
});

/* const ComponentSchema = new Schema({
    componentName: String,
    componentColor: String,
    productID: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
    componentLocation: String,
    createTime: {
        type: Date,
        default: Date.now
    }
}); */


export default mongoose.model('Component', ComponentSchema);