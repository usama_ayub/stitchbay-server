import * as mongoose from 'mongoose';
import Product from '../../app/model/product'

const Schema = mongoose.Schema;

const MeasurementComponentSchema = new Schema({
    product_id: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
    measurement_name: String,
    measurement_img: String,
    measurement_gif: String,
    measurement_location: String,
    create_time: {
        type: Date,
        default: Date.now
    }
});



export default mongoose.model('MeasurementComponent', MeasurementComponentSchema);