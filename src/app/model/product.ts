import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    product_name: String,
    product_desc: String,
    product_location: String,
    create_time: {
        type: Date,
        default: Date.now
    }
});



export default mongoose.model('Product', ProductSchema);