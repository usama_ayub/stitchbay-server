import * as mongoose from 'mongoose';
import Product from '../../app/model/product';
import Component from '../../app/model/component';
import User from '../../app/model/user';
import DesignComponent from '../../app/model/designComponent';
import UserMeasurement from '../../app/model/userMeasurement';

const Schema = mongoose.Schema;

const UserProductSchema = new Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    product_id: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
    //component_id: { type: Schema.Types.ObjectId, ref: 'Component', required: true },
    designComponent: [{
        _id: {
            type: Schema.Types.ObjectId,
            ref: 'DesignComponent',
            required: true
        }
    }],
    userMeasurement_id: { type: Schema.Types.ObjectId, ref: 'UserMeasurement', required: true },
    cost: Number,
    create_time: {
        type: Date,
        default: Date.now
    }
});



export default mongoose.model('UserProduct', UserProductSchema);