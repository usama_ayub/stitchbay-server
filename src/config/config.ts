let dbuser = 'admin';
let dbpass = 'admin';
let dbname = 'test-db';
export default {
  jwtSecret: 'mySecret',
  mongoURL: process.env.MONGODB_URI || `mongodb://${dbuser}:${dbuser}@ds129610.mlab.com:29610/${dbname}`,
  port: process.env.PORT || 3000
};

